package net.chuckc.sensortest.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import java.util.List;
import java.util.ArrayList;

import net.chuckc.sensortest.activity.SensorActivity;
import net.chuckc.sensortest.sensor.AccelerometerService;
import net.chuckc.sensortest.sensor.AccelerometerResultReceiver;
import net.chuckc.sensortest.sensor.SensorItem;
import net.chuckc.sensortest.sensor.SensorListAdapter;

public class MainActivity extends Activity implements OnItemClickListener {
    AccelerometerResultReceiver accelReceiver;
    SensorManager sensorMgr;
    ListView lvSensors;
    LinearLayout mainLayout;
    Intent sensorIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorMgr = (SensorManager)getSystemService(SENSOR_SERVICE);
        setup();
    }

    private void setup() {
        List<Sensor> sensorList = sensorMgr.getSensorList(Sensor.TYPE_ALL);
        ArrayList lvList = new ArrayList();
        SensorItem tmpSensor;
        for (int i = 0; i < sensorList.size(); i++) {
            tmpSensor = new SensorItem(sensorList.get(i).getName(), sensorList.get(i).getType());
            lvList.add(tmpSensor);
        }

        lvSensors = new ListView(this);
        lvSensors.setAdapter(new SensorListAdapter(this, lvList));
        lvSensors.setOnItemClickListener(MainActivity.this);
        mainLayout = new LinearLayout(this);
        mainLayout.addView(lvSensors);

        setContentView(mainLayout);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        SensorItem s = (SensorItem)lvSensors.getItemAtPosition(pos);
        sensorIntent = new Intent(MainActivity.this, SensorActivity.class);
        startActivity(sensorIntent);
    }

}
