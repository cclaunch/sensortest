package net.chuckc.sensortest.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.chuckc.sensortest.sensor.SensorItem;
import net.chuckc.sensortest.sensor.AccelerometerResultReceiver;
import net.chuckc.sensortest.sensor.AccelerometerService;
import net.chuckc.sensortest.util.Constants;

public class SensorActivity extends Activity {
    AccelerometerResultReceiver accelReceiver;
    TextView tvDisplay;
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvDisplay = new TextView(this);
        mainLayout = new LinearLayout(this);
        mainLayout.addView(tvDisplay);
        setContentView(mainLayout);
        // setupSensor();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopService(new Intent(this, AccelerometerService.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        setupSensor();
    }

    private void setupSensor() {
        Handler handler = new Handler();
        //! @Todo later do type check of input sensor first
        accelReceiver = new AccelerometerResultReceiver(handler);
        accelReceiver.setReceiver(new AccelerometerReceiver());
        if (accelReceiver != null) {
            Intent accelIntent = new Intent(SensorActivity.this, AccelerometerService.class);
            accelIntent.putExtra(Constants.EXTRA_RECEIVER, accelReceiver);
            startService(accelIntent);
        } else {
            Toast.makeText(SensorActivity.this, "Doh!", Toast.LENGTH_SHORT).show();
        }
    }

    private class AccelerometerReceiver implements AccelerometerResultReceiver.Receiver {

        @Override
        public void newEvent(float x, float y, float z) {
            double roundx = Math.round(x);
            double roundy = Math.round(y);
            double roundz = Math.round(z);
            tvDisplay.setText("x: " + roundx + "\ny: " + roundy + "\nz: " + roundz);
        }

        @Override
        public void error(String error) {

        }
    }
}